#![warn(clippy::all)]
#![feature(custom_attribute)]
#![feature(uniform_paths)]
pub mod codec;
pub mod collaboration;
pub mod identity;
pub mod transport;

#[test]
fn it_works() {
    collaboration::do_the_thing().unwrap_or_else(|err| println!("{}", err));
}
