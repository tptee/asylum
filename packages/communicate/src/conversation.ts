import * as hyperswarm from '@hyperswarm/network';
import * as mississippi from 'mississippi';

import * as events from 'events';

import LocalMember from './local-member';
import RemoteMember from './remote-member';

export default class Conversation extends events.EventEmitter {
  sessionId: string;
  localMember: LocalMember;
  remoteMemberDiscoveryKeys: Array<Buffer>;
  remoteMembers: Array<RemoteMember>;

  private network: typeof hyperswarm;

  constructor({
    sessionId,
    localMember,
    remoteMemberDiscoveryKeys,
    network
  }: {
    sessionId: string;
    localMember: LocalMember;
    remoteMemberDiscoveryKeys: Array<Buffer>;
    network: typeof hyperswarm;
  }) {
    super();
    this.sessionId = sessionId;
    this.localMember = localMember;
    this.remoteMembers = [];
    this.remoteMemberDiscoveryKeys = remoteMemberDiscoveryKeys;
    this.network = network;

    for (const discoveryKey of this.remoteMemberDiscoveryKeys) {
      this.network.join(discoveryKey, {
        lookup: true,
        announce: false
      });
    }
  }
}
