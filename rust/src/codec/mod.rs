use actix::Message;
use byteorder::{ByteOrder, NetworkEndian};
use bytes::Bytes;
use bytes::{BufMut, BytesMut};
use failure::Fail;
use std::io;
use tokio_codec::{Decoder, Encoder};

#[derive(Message, Debug)]
pub enum NoiseMessage {
  Bytes(Bytes),
  HandshakeComplete,
  Empty,
}

#[derive(Fail, Debug)]
#[fail(display = "An error occurred.")]
pub enum CodecError {
  #[fail(display = "IO error: {}", _0)]
  Io(io::Error),
}

impl From<io::Error> for CodecError {
  fn from(err: io::Error) -> Self {
    CodecError::Io(err)
  }
}

#[derive(Default)]
pub struct Codec {}

impl Codec {
  pub fn new() -> Codec {
    Codec {}
  }
}

impl Decoder for Codec {
  type Item = NoiseMessage;
  type Error = CodecError;

  fn decode(&mut self, src: &mut BytesMut) -> Result<Option<Self::Item>, Self::Error> {
    println!("decoding {:#?}", src);
    let size = {
      if src.len() < 2 {
        return Ok(None);
      }
      NetworkEndian::read_u16(src.as_ref()) as usize
    };

    if src.len() >= size + 2 {
      src.split_to(2);
      let buf = src.split_to(size);
      return Ok(Some(NoiseMessage::Bytes(buf.freeze())));
    }
    Ok(None)
  }
}

impl Encoder for Codec {
  type Item = NoiseMessage;
  type Error = CodecError;

  fn encode(&mut self, item: Self::Item, dst: &mut BytesMut) -> Result<(), Self::Error> {
    let item = match item {
      NoiseMessage::Bytes(bytes) => Ok(bytes),
      _ => Err("Unsupported message"),
    }
    .unwrap();

    println!("encoding {:x?}", item);
    dst.reserve(item.len() + 2);
    dst.put_u16_be(item.len() as u16);
    dst.put(item);

    Ok(())
  }
}
