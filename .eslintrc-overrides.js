module.exports = {
  parser: 'babel-eslint',
  plugins: ['babel'],
  rules: {
    'valid-jsdoc': 'off',
    'no-magic-numbers': 'off',
    'react/no-multi-comp': 'off',
    'react/sort-prop-types': 'off',
    'react/jsx-handler-names': 'off',
    'babel/no-invalid-this': 1,
    'no-invalid-this': 'off'
  }
};
