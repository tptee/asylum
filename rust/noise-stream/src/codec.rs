use byteorder::{ByteOrder, NetworkEndian};
use bytes::{BufMut, Bytes, BytesMut};
use std::io;
use tokio_codec::{Decoder, Encoder};

pub(crate) struct NoiseCodec;

impl Decoder for NoiseCodec {
  type Item = Bytes;
  type Error = io::Error;

  fn decode(
    &mut self,
    src: &mut BytesMut,
  ) -> Result<Option<Self::Item>, Self::Error> {
    let size = {
      if src.len() < 2 {
        return Ok(None);
      }
      NetworkEndian::read_u16(src.as_ref()) as usize
    };

    if src.len() >= size + 2 {
      src.split_to(2);
      let buf = src.split_to(size);
      return Ok(Some(buf.freeze()));
    }
    Ok(None)
  }
}

impl Encoder for NoiseCodec {
  type Item = Bytes;
  type Error = io::Error;

  fn encode(
    &mut self,
    item: Self::Item,
    dst: &mut BytesMut,
  ) -> Result<(), Self::Error> {
    dst.reserve(item.len() + 2);
    dst.put_u16_be(item.len() as u16);
    dst.put(item);

    Ok(())
  }
}
