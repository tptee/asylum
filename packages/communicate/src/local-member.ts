import { promisifyAll } from 'bluebird';
import { Cryptobox } from '@wireapp/cryptobox';
import { FileEngine } from '@wireapp/store-engine/dist/commonjs/engine';
import * as path from 'path';
import * as sodium from 'libsodium-wrappers-sumo';
import * as hyperdb from 'hyperdb';
import { pipe } from 'mississippi';
import { Socket } from 'net';

export default class LocalMember {
  feed: any;
  cryptobox: Cryptobox;
  network: any;

  private constructor({
    feed,
    cryptobox,
    network
  }: {
    feed: any;
    cryptobox: Cryptobox;
    network: any;
  }) {
    this.feed = feed;
    this.cryptobox = cryptobox;
    this.network = network;
  }

  public static async create({
    deviceId,
    network
  }: {
    deviceId: string;
    network: any;
  }) {
    const engine = new FileEngine(path.resolve(process.cwd(), deviceId));
    await Promise.all([
      engine.init(deviceId, { fileExtension: '.db' }),
      sodium.ready
    ]);
    const cryptobox = new Cryptobox(engine, 50);
    await cryptobox.create();
    const feed = promisifyAll(
      hyperdb(path.resolve(process.cwd(), deviceId), {
        valueEncoding: 'binary'
      })
    );
    return new Promise((resolve: (member: LocalMember) => void) => {
      const member = new LocalMember({ feed, cryptobox, network });
      member.feed.on('ready', () => resolve(member));
    });
  }

  public announce() {
    this.network.join(this.feed.discoveryKey, {
      lookup: false,
      announce: true
    });

    this.network.on('connection', (socket: Socket, details: any) => {
      socket.write(this.feed.local.key);
      socket.once('data', remoteKey => {
        const remoteFeed = promisifyAll(
          hyperdb(
            path.resolve(process.cwd(), 'remote', remoteKey.toString('hex')),
            remoteKey,
            {
              valueEncoding: 'binary'
            }
          )
        );
        remoteFeed.on('ready', () => {
          // Listener
          if (!details.client) {
            const localStream = this.feed.replicate({ live: true });
            const remoteStream = remoteFeed.replicate({
              live: true,
              stream: localStream
            });
            pipe(
              remoteStream,
              socket,
              remoteStream
            );
            return;
          }

          // Dialer
          const remoteStream = remoteFeed.replicate({ live: true });
          const localStream = this.feed.replicate({
            live: true,
            stream: remoteStream
          });
          pipe(
            localStream,
            socket,
            localStream
          );
        });
      });
    });
  }
}
