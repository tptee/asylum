#![warn(clippy::all)]
#![feature(uniform_paths)]

use bytes::BytesMut;
use futures::prelude::*;
use snow::Session;
use std::fmt;
use std::io;
use std::io::{Read, Result, Write};
use tokio_codec::{Decoder, Encoder};
use tokio_io::{AsyncRead, AsyncWrite};

mod codec;
use codec::NoiseCodec;

#[derive(Debug)]
pub enum NoiseStreamError {
    Snow(snow::SnowError),
}

impl fmt::Display for NoiseStreamError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "Snow error fam")
    }
}

impl std::error::Error for NoiseStreamError {
    fn description(&self) -> &str {
        "Noise stream error"
    }

    fn cause(&self) -> Option<&std::error::Error> {
        None
    }
}

#[derive(Debug)]
enum PeerHandshakeKind {
    Initiator(InitiatorState),
    Responder(ResponderState),
}

// State machine events for both handshakes
#[derive(Debug)]
struct NeedsWrite;
#[derive(Debug)]
struct NeedsRead;
#[derive(Debug)]
struct Ready;

#[derive(Debug)]
struct InitiatorMachine<S> {
    state: S,
}

impl From<InitiatorMachine<NeedsWrite>> for InitiatorMachine<NeedsRead> {
    fn from(_: InitiatorMachine<NeedsWrite>) -> InitiatorMachine<NeedsRead> {
        InitiatorMachine { state: NeedsRead }
    }
}

impl From<InitiatorMachine<NeedsRead>> for InitiatorMachine<Ready> {
    fn from(_: InitiatorMachine<NeedsRead>) -> InitiatorMachine<Ready> {
        InitiatorMachine { state: Ready }
    }
}

#[derive(Debug)]
enum InitiatorState {
    NeedsWrite(InitiatorMachine<NeedsWrite>),
    NeedsRead(InitiatorMachine<NeedsRead>),
    Ready(InitiatorMachine<Ready>),
}
impl InitiatorState {
    fn new() -> Self {
        InitiatorState::NeedsWrite(InitiatorMachine { state: NeedsWrite })
    }
    fn step(self) -> Self {
        use InitiatorState::*;
        match self {
            NeedsWrite(state) => NeedsRead(state.into()),
            NeedsRead(state) => Ready(state.into()),
            Ready(state) => Ready(state),
        }
    }
}

#[derive(Debug)]
struct ResponderMachine<S> {
    state: S,
}

impl From<ResponderMachine<NeedsRead>> for ResponderMachine<NeedsWrite> {
    fn from(_: ResponderMachine<NeedsRead>) -> ResponderMachine<NeedsWrite> {
        ResponderMachine { state: NeedsWrite }
    }
}

impl From<ResponderMachine<NeedsWrite>> for ResponderMachine<Ready> {
    fn from(_: ResponderMachine<NeedsWrite>) -> ResponderMachine<Ready> {
        ResponderMachine { state: Ready }
    }
}

#[derive(Debug)]
enum ResponderState {
    NeedsRead(ResponderMachine<NeedsRead>),
    NeedsWrite(ResponderMachine<NeedsWrite>),
    Ready(ResponderMachine<Ready>),
}
impl ResponderState {
    fn new() -> Self {
        ResponderState::NeedsRead(ResponderMachine { state: NeedsRead })
    }
    fn step(self) -> Self {
        use ResponderState::*;
        match self {
            NeedsRead(state) => NeedsWrite(state.into()),
            NeedsWrite(state) => Ready(state.into()),
            Ready(state) => Ready(state),
        }
    }
}

enum HandshakeStatus {
    DidWrite(usize),
    DidRead(usize),
    Complete,
}

type HandshakeResult = Result<HandshakeStatus>;

pub struct NoiseStream<S: Read + Write> {
    stream: S,
    session: Option<Session>,
    peer_handshake_kind: Option<PeerHandshakeKind>,
}

impl<S: Read + Write> NoiseStream<S> {
    pub fn new(session: Session, stream: S) -> Self {
        let peer_handshake_kind = if session.is_initiator() {
            Some(PeerHandshakeKind::Initiator(InitiatorState::new()))
        } else {
            Some(PeerHandshakeKind::Responder(ResponderState::new()))
        };
        NoiseStream {
            stream,
            session: Some(session),
            peer_handshake_kind,
        }
    }

    fn handshake(&mut self) -> HandshakeResult {
        let mut result;

        loop {
            result = self.turn();

            match result {
                Ok(HandshakeStatus::Complete) => {
                    self.session = Some(
                        self.session
                            .take()
                            .unwrap()
                            .into_transport_mode()
                            .unwrap(),
                    );
                    return Ok(HandshakeStatus::Complete);
                }
                Err(err) => return Err(err),
                Ok(_) => {}
            };
        }
    }

    fn turn(&mut self) -> HandshakeResult {
        use PeerHandshakeKind::*;

        if self.session.as_mut().unwrap().is_handshake_finished() {
            return Ok(HandshakeStatus::Complete);
        }

        match self.peer_handshake_kind.take() {
            Some(Initiator(state)) => match state {
                InitiatorState::NeedsWrite(_) => {
                    let result = self.write_noise(&[]);
                    if result.is_ok() {
                        println!("(1) initiator write success");
                        self.peer_handshake_kind =
                            Some(Initiator(state.step()));
                    } else {
                        self.peer_handshake_kind = Some(Initiator(state));
                    }
                    result.map(HandshakeStatus::DidWrite)
                }
                InitiatorState::NeedsRead(_) => {
                    let result = self.read_noise(&mut []);
                    if result.is_ok() {
                        println!("(4) initiator read success");
                        self.peer_handshake_kind =
                            Some(Initiator(state.step()));
                    } else {
                        self.peer_handshake_kind = Some(Initiator(state));
                    }
                    result.map(HandshakeStatus::DidRead)
                }
                InitiatorState::Ready(_) => Ok(HandshakeStatus::Complete),
            },
            Some(Responder(state)) => match state {
                ResponderState::NeedsRead(_) => {
                    let result = self.read_noise(&mut []);
                    if result.is_ok() {
                        println!("(2) responder read success");
                        self.peer_handshake_kind =
                            Some(Responder(state.step()));
                    } else {
                        self.peer_handshake_kind = Some(Responder(state));
                    }
                    result.map(HandshakeStatus::DidRead)
                }
                ResponderState::NeedsWrite(_) => {
                    let result = self.write_noise(&[]);
                    if result.is_ok() {
                        println!("(3) responder write success");
                        self.peer_handshake_kind =
                            Some(Responder(state.step()));
                    } else {
                        self.peer_handshake_kind = Some(Responder(state));
                    }
                    result.map(HandshakeStatus::DidWrite)
                }
                ResponderState::Ready(_) => Ok(HandshakeStatus::Complete),
            },
            _ => panic!(
                "Ran take() on self.peer_handshake_kind without reassigning"
            ),
        }
    }

    fn read_noise(&mut self, buf: &mut [u8]) -> Result<usize> {
        let mut tcp_buf = [0u8; 200];
        let len = self.stream.read(&mut tcp_buf)?;
        let unframed_bytes = NoiseCodec
            .decode(&mut tcp_buf[..len].into())?
            .ok_or_else(|| {
                io::Error::new(
                    io::ErrorKind::InvalidData,
                    "Could not find valid frame in data.",
                )
            })?;
        let result = self
            .session
            .as_mut()
            .unwrap()
            .read_message(&unframed_bytes[..], buf)
            .map_err(|err| {
                println!("{}", err);
                io::Error::new(
                    io::ErrorKind::InvalidInput,
                    NoiseStreamError::Snow(err),
                )
            })?;

        Ok(result)
    }

    fn write_noise(&mut self, buf: &[u8]) -> Result<usize> {
        let mut noise_buf = [0u8; 65565];
        let len = self
            .session
            .as_mut()
            .unwrap()
            .write_message(buf, &mut noise_buf)
            .map_err(|err| {
                io::Error::new(
                    io::ErrorKind::InvalidInput,
                    NoiseStreamError::Snow(err),
                )
            })?;

        let mut tcp_buffer = BytesMut::new();
        NoiseCodec.encode(noise_buf[..len].into(), &mut tcp_buffer)?;
        let result = self.stream.write(&tcp_buffer);
        println!("write impl bytes written:, {:#?}", result);
        result
    }
}

impl<S: Read + Write> Read for NoiseStream<S> {
    fn read(&mut self, buf: &mut [u8]) -> Result<usize> {
        self.handshake()?;
        self.read_noise(buf)
    }
}

impl<S: Read + Write> Write for NoiseStream<S> {
    fn write(&mut self, buf: &[u8]) -> Result<usize> {
        self.handshake()?;
        self.write_noise(buf)
    }

    fn flush(&mut self) -> Result<()> {
        self.stream.flush()
    }
}

impl<S: Read + Write> AsyncRead for NoiseStream<S> {}

impl<S: Read + Write + AsyncWrite> AsyncWrite for NoiseStream<S> {
    fn shutdown(&mut self) -> Poll<(), io::Error> {
        self.session = None;
        AsyncWrite::shutdown(&mut self.stream)
    }
}

#[cfg(test)]
mod test {
    use super::*; // import parent stuff to test
    use bytes::Bytes;
    use hex::decode;
    use snow::Builder;
    use std::io::{Read, Write};
    use std::net::SocketAddr;
    use std::str::FromStr;
    use tokio::run;
    use tokio_codec::{BytesCodec, Framed};
    use tokio_tcp::{TcpListener, TcpStream};

    #[test]
    fn can_complete_handshake_sync() {
        let noise_params = "Noise_IK_25519_ChaChaPoly_BLAKE2s";

        let initiator_session = Builder::new(noise_params.parse().unwrap())
            .local_private_key(
                &decode("7be0df71f7656eeb9f45b8431b7013a1744ef49ddddc336720881520bc15c18a")
                    .expect("Bad private key hex"),
            )
            .remote_public_key(
                &decode("fcb3405a3be2fea06878b410789990ee4a7ab31c8ea05abf51be57b84cd1a73b")
                    .expect("Bad public key hex"),
            )
            .build_initiator()
            .expect("could not create initiator session!");

        let responder_session = Builder::new(noise_params.parse().unwrap())
            .local_private_key(
                &decode("357c5bc003365b7199bae68402498faea5108543c5c09352ba57a2ec40cef0ef")
                    .expect("Bad private key hex"),
            )
            .remote_public_key(
                &decode("5ebfdd802a596e0802ede7d22caaa25dbaf50c0f7eab7a8f307266eb29522767")
                    .expect("Bad public key hex"),
            )
            .build_responder()
            .expect("could not create responder session!");

        let listener = std::net::TcpListener::bind("127.0.0.1:1234")
            .expect("Couldn't bind to port 1234.");

        let _ = std::thread::spawn(move || {
            let tcp_stream =
                std::net::TcpStream::connect("127.0.0.1:1234").unwrap();
            let mut stream = NoiseStream::new(initiator_session, tcp_stream);
            let _ = stream.write(b"Hello").unwrap();
            let buf = &mut [0u8; 65565];
            let len = stream.read(buf).unwrap();
            assert!(buf[..len] == b"world"[..]);
        });

        let tcp_stream = listener.accept().unwrap().0;
        let mut stream = NoiseStream::new(responder_session, tcp_stream);
        let buf = &mut [0u8; 65565];
        let len = stream.read(buf).unwrap();
        assert!(buf[..len] == b"Hello"[..]);
        let _ = stream.write(b"world").unwrap();
    }

    #[test]
    fn can_complete_handshake_async() {
        let noise_params = "Noise_IK_25519_ChaChaPoly_BLAKE2s";

        let addr = SocketAddr::from_str("127.0.0.1:9999").unwrap();

        std::thread::spawn(move || {
            let listener = TcpListener::bind(&addr).unwrap();

            let server = listener
                .incoming()
                .map(move |socket| {
                    let responder_session = Builder::new(noise_params.parse().unwrap())
                        .local_private_key(
                            &decode(
                                "357c5bc003365b7199bae68402498faea5108543c5c09352ba57a2ec40cef0ef",
                            )
                            .expect("Bad private key hex"),
                        )
                        .remote_public_key(
                            &decode(
                                "5ebfdd802a596e0802ede7d22caaa25dbaf50c0f7eab7a8f307266eb29522767",
                            )
                            .expect("Bad public key hex"),
                        )
                        .build_responder()
                        .expect("could not create responder session!");

                    let server_stream = NoiseStream::new(responder_session, socket);
                    Framed::new(server_stream, BytesCodec::new())
                })
                .for_each(|transport| {
                    let read_messages = transport
                        .for_each(|message| {
                            println!("server received message: {:x?}", message);
                            Ok(())
                        })
                        .map_err(|err| {
                            println!("{:#?}", err);
                        });
                    tokio::spawn(read_messages);
                    Ok(())
                })
                .map_err(|err| {
                    println!("{:#?}", err);
                });

            run(server);
        });

        let initiator_session = Builder::new(noise_params.parse().unwrap())
            .local_private_key(
                &decode("7be0df71f7656eeb9f45b8431b7013a1744ef49ddddc336720881520bc15c18a")
                    .expect("Bad private key hex"),
            )
            .remote_public_key(
                &decode("fcb3405a3be2fea06878b410789990ee4a7ab31c8ea05abf51be57b84cd1a73b")
                    .expect("Bad public key hex"),
            )
            .build_initiator()
            .expect("could not create initiator session!");

        let client = TcpStream::connect(&addr)
            .map_err(|err| {
                println!("{:#?}", err);
            })
            .and_then(|socket| {
                println!("about to write from client to server");
                let client_stream = NoiseStream::new(initiator_session, socket);
                Ok(Framed::new(client_stream, BytesCodec::new()))
            })
            .and_then(|transport| {
                transport.send(Bytes::from(&b"whatuuuuuuup"[..])).map_err(
                    |err| {
                        println!("{:#?}", err);
                    },
                )
            })
            .map_err(|err| {
                println!("{:#?}", err);
            })
            .and_then(|_| {
                std::thread::sleep(std::time::Duration::from_secs(10));
                Ok(())
            });

        run(client);
    }
}
