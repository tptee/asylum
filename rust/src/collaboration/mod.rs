use cryptobox::CBox;
use ditto::List;
use proteus::keys::PreKeyId;
use std::error::Error;
use std::sync::Arc;
use tokio::prelude::*;

// Starting a collaboration:
// - create content CRDT
// - add local peer with a site id of 0

// Adding a peer to a collaboration:
// - add remote peer with a site id of last_site_id++

// Opening a collaboration:
// - open and deserialize the collaboration from a file
// - open and deserialize referenced local and remote peers from a file

pub trait Collaboration<T> {
    fn assign_local_peer(&mut self, peer: String);
    fn replicate(
        &self,
        peer: String,
        index: Option<u32>,
    ) -> Box<
        Stream<
            Item = Result<Vec<u8>, rmp_serde::encode::Error>,
            Error = Box<Error>,
        >,
    >;
}

pub struct Conversation {
    owner: String,
    _history: List<Vec<u8>>,
    oplog: Arc<Vec<ditto::list::Op<std::vec::Vec<u8>>>>,
}
impl Collaboration<ditto::list::Op<std::vec::Vec<u8>>> for Conversation {
    fn assign_local_peer(&mut self, peer: String) {
        self.owner = peer
    }
    fn replicate<'a>(
        &self,
        _peer: String,
        _index: Option<u32>,
    ) -> Box<
        Stream<
            Item = Result<Vec<u8>, rmp_serde::encode::Error>,
            Error = Box<Error>,
        >,
    > {
        let oplog = Arc::clone(&self.oplog);
        let len = self.oplog.len();
        // http://smallcultfollowing.com/babysteps/blog/2018/09/02/rust-pattern-iterating-an-over-a-rc-vec-t/
        Box::new(stream::iter_ok(
            (0..len).map(move |i| rmp_serde::to_vec(&oplog[i])),
        ))
    }
}

pub struct Oplog<T> {
    _log: Vec<T>,
    iter: Box<Iterator<Item = Result<Vec<u8>, rmp_serde::encode::Error>>>,
}

impl<T: serde::ser::Serialize + 'static> Oplog<T> {
    fn new(&mut self, ops: Vec<T>) {
        let len = ops.len();
        let oplog_iter = (0..len).map(move |i| rmp_serde::to_vec(&ops[i]));
        self.iter = Box::new(oplog_iter);
        // self.o = ops;
        // self.ops = self.o.iter();
    }
}

// impl<T> Stream for Oplog<T> {
//     type Item = &T;
//     type Error = Box<Error>;

//     fn poll(&mut self) -> Poll<Option<Self::Item>, Self::Error> {
//         self.iter.next().into()
//         Ok(Async::Ready(self.iter.next()))
//     }
// }

pub fn do_the_thing() -> Result<(), Box<Error>> {
    let alice = CBox::file_open("./alice")?;
    let bob = CBox::file_open("./bob")?;

    let mut alice_list: List<Vec<u8>> = List::from(Vec::new());
    let mut bob_list: List<Vec<u8>> = List::from(Vec::new());

    let bob_prekey = bob.new_prekey(PreKeyId::new(0))?.serialise()?;

    let mut alice_session =
        alice.session_from_prekey(String::from("asylum"), &bob_prekey)?;
    let alice_message_to_bob =
        alice_session.encrypt(String::from("hello bob").as_bytes())?;

    let alice_op = alice_list.push(alice_message_to_bob).unwrap();
    let serialized_alice_op = rmp_serde::to_vec(&alice_op)?;

    let deserialized_alice_op: ditto::list::Op<std::vec::Vec<u8>> =
        rmp_serde::from_slice(&serialized_alice_op)?;
    bob_list.execute_op(deserialized_alice_op);

    assert!(bob_list == alice_list);

    // let (mut bob_session, message) =
    //     bob.session_from_message(String::from("asylum"), &bob_list.get(0).unwrap())?;

    // println!("{}", String::from_utf8(message)?);

    Ok(())
}
