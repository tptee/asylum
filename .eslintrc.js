module.exports = {
  extends: [
    'formidable/configurations/es6-node',
    'prettier',
    './.eslintrc-overrides.js'
  ],
  overrides: [
    {
      files: ['packages/**/src/**/*.test.js'],
      env: {
        jest: true
      },
      globals: {
        renderWithTheme: true,
        wrapWithTheme: true,
        shallow: true,
        mount: true,
        render: true
      }
    }
  ]
};
