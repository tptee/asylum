'use strict';

import * as path from 'path';
import { Cryptobox } from '@wireapp/cryptobox';
import * as sodium from 'libsodium-wrappers-sumo';
import FileEngine from '@wireapp/store-engine/dist/commonjs/engine/FileEngine';
import * as hyperdb from 'hyperdb';
import * as file from 'random-access-file';
import { promisifyAll } from 'bluebird';
import * as network from '@hyperswarm/network';

export default class Device {
  id: string;
  cryptobox: Cryptobox;
  db: any;
  network: any;

  constructor({
    id,
    cryptobox,
    db
  }: {
    id: string;
    cryptobox: Cryptobox;
    db: any;
  }) {
    this.id = id;
    this.cryptobox = cryptobox;
    this.db = db;
    this.network = network();
  }

  public static async create({ deviceId }: { deviceId: string }) {
    const engine = new FileEngine(path.resolve(process.cwd(), deviceId));
    await Promise.all([
      engine.init(deviceId, { fileExtension: '.db' }),
      sodium.ready
    ]);
    const cryptobox = new Cryptobox(engine, 50);
    try {
      await cryptobox.create();
      console.log('created');
    } catch (e) {
      console.error(e);
    }
    console.log('here');
    const db = promisifyAll(hyperdb(file, { valueEncoding: 'binary' }));
    return new Device({ id: deviceId, cryptobox, db });
  }

  public async send(
    sessionId: string,
    payload: string | Uint8Array,
    preKeyBundle?: ArrayBuffer
  ) {
    const ciphertext = await this.cryptobox.encrypt(
      sessionId,
      payload,
      preKeyBundle
    );
    await this.db.put(`/outbox/${sessionId}`, Buffer.from(ciphertext));
  }

  public async receive(sessionId: string, ciphertext: ArrayBuffer) {
    const plaintext = await this.cryptobox.decrypt(sessionId, ciphertext);
    await this.db.put(`/inbox/${sessionId}`, Buffer.from(plaintext));
  }

  public async publish() {
    if (!this.cryptobox.identity) {
      throw new Error('Identity must exist before publishing!');
    }
    await this.db.put(
      `/announce/${this.cryptobox.identity.public_key.fingerprint()}`,
      await this.cryptobox.get_prekey_bundle()
    );
  }

  public async announce() {
    if (!this.cryptobox.identity) {
      throw new Error('Identity must exist before publishing!');
    }
    this.network.join(
      Buffer.from(this.cryptobox.identity.public_key.fingerprint()),
      {
        lookup: true,
        announce: true
      }
    );

    console.log('looking');
    this.network.on('connection', (socket: any, details: any) => {
      console.log('new connection!', details, socket);

      // you can now use the socket as a stream, eg:
      // process.stdin.pipe(socket).pipe(process.stdout)
    });
  }
}
