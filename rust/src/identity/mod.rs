use ::actix::prelude::*;
use failure::Fail;
use futures::future::Future;
use lazy_static::lazy_static;
use snow::params::NoiseParams;
use snow::{Builder, Keypair};
use std::io;
use std::path::PathBuf;
use std::sync::Arc;
use tokio_fs::file::File;
use tokio_io::io::read_to_end;
use tokio_io::AsyncWrite;
use tokio_threadpool::ThreadPool;

lazy_static! {
  static ref NOISE_PARAMS: NoiseParams = "Noise_IK_25519_ChaChaPoly_BLAKE2s".parse().unwrap();
}

#[derive(Fail, Debug)]
#[fail(display = "An error occurred.")]
pub enum IdentityError {
  #[fail(display = "Failed to create keypair: {}", _0)]
  KeypairGeneration(snow::SnowError),
  #[fail(display = "Could not acquire exclusive reference to keypair")]
  KeypairAcquire(),
  #[fail(display = "IO error: {}", _0)]
  Io(io::Error),
}

pub struct IdentityActor {
  id: Arc<String>,
  pool: ThreadPool,
}

impl IdentityActor {
  pub fn new(id: String) -> Self {
    IdentityActor {
      id: Arc::new(id),
      pool: tokio_threadpool::Builder::new()
        .pool_size(num_cpus::get())
        .build(),
    }
  }
}

impl Actor for IdentityActor {
  type Context = Context<Self>;
}

pub struct GetTransportKeypair;

impl Message for GetTransportKeypair {
  type Result = Result<snow::Keypair, IdentityError>;
}

impl Handler<GetTransportKeypair> for IdentityActor {
  type Result = ResponseFuture<snow::Keypair, IdentityError>;

  fn handle(&mut self, _msg: GetTransportKeypair, _ctx: &mut Self::Context) -> Self::Result {
    let id = Arc::clone(&self.id);
    let get_file = self.pool.spawn_handle(futures::lazy(move || {
      futures::Future::join(
        File::open(PathBuf::from(format!("./src/identity/{}/public", id)))
          .and_then(|file| read_to_end(file, vec![]))
          .map(|(_, buffer)| buffer),
        File::open(PathBuf::from(format!("./src/identity/{}/private", id)))
          .and_then(|file| read_to_end(file, vec![]))
          .map(|(_, buffer)| buffer),
      )
      .map_err(IdentityError::Io)
      .map(|(public, private)| Keypair { public, private })
      .or_else(|_| {
        println!("Transport keypair missing, creating new keypair...");
        Builder::new(NOISE_PARAMS.clone()).generate_keypair()
      })
      .map_err(IdentityError::KeypairGeneration)
      .and_then(move |keypair| {
        let keypair = Arc::new(keypair);
        let public_file_keypair = Arc::clone(&keypair);
        let private_file_keypair = Arc::clone(&keypair);
        futures::Future::join3(
          futures::future::ok(keypair),
          File::create(PathBuf::from(format!("./src/identity/{}/public", id)))
            .and_then(move |mut file| file.poll_write(&public_file_keypair.public)),
          File::create(PathBuf::from(format!("./src/identity/{}/private", id)))
            .and_then(move |mut file| file.poll_write(&private_file_keypair.private)),
        )
        .map_err(IdentityError::Io)
      })
      .and_then(|(keypair, _, _)| {
        Arc::try_unwrap(keypair).map_err(|_| IdentityError::KeypairAcquire())
      })
    }));

    Box::new(get_file)
  }
}

pub struct SaveTransportKeypair(snow::Keypair);

impl Message for SaveTransportKeypair {
  type Result = Result<(), io::Error>;
}

impl Handler<SaveTransportKeypair> for IdentityActor {
  type Result = ResponseFuture<(), io::Error>;

  fn handle(&mut self, msg: SaveTransportKeypair, _ctx: &mut Self::Context) -> Self::Result {
    let id = Arc::clone(&self.id);
    let public = msg.0.public;
    let private = msg.0.private;

    let save_file = self.pool.spawn_handle(futures::lazy(move || {
      futures::Future::join(
        File::create(PathBuf::from(format!("./src/identity/{}/public", id)))
          .and_then(move |mut file| file.poll_write(&public)),
        File::create(PathBuf::from(format!("./src/identity/{}/private", id)))
          .and_then(move |mut file| file.poll_write(&private)),
      )
      .and_then(|_| futures::future::ok(()))
    }));

    Box::new(save_file)
  }
}
