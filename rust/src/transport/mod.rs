use bytes::Bytes;
use failure::{Error, Fail};
use futures::prelude::*;
use futures::{future, try_ready};
use snow::Session;
use std::io;
use std::net::SocketAddr;
use tokio_codec::{FramedRead, FramedWrite};
use tokio_io::io::{ReadHalf, WriteHalf};
use tokio_io::AsyncRead;
use tokio_tcp::{TcpListener, TcpStream};

use crate::codec::{Codec, CodecError, NoiseMessage};

#[derive(Fail, Debug)]
#[fail(display = "An error occurred.")]
pub enum NoiseTransportError {
  #[fail(display = "IO error: {}", _0)]
  Io(io::Error),
  #[fail(display = "Codec error: {:?}", _0)]
  Codec(CodecError),
}

type Reader = FramedRead<ReadHalf<TcpStream>, Codec>;
type Writer = FramedWrite<WriteHalf<TcpStream>, Codec>;

pub struct NoiseTransport {
  session: Session,
  reader: Reader,
  writer: Writer,
}

impl NoiseTransport {
  pub fn connect(
    addr: &SocketAddr,
    mut session: Session,
  ) -> impl Future<Item = NoiseTransport, Error = Error> {
    TcpStream::connect(&addr)
      .map_err(Error::from)
      .and_then(|socket| {
        let (read, write) = socket.split();
        let reader = FramedRead::new(read, Codec::new());
        let writer = FramedWrite::new(write, Codec::new());

        let mut buf = [0u8; 65565];
        let len = session.write_message(&[], &mut buf).unwrap();

        Future::join3(
          future::ok(reader),
          writer
            .send(NoiseMessage::Bytes(Bytes::from(&buf[..len])))
            .map_err(Error::from),
          future::ok(session),
        )
      })
      .and_then(|(reader, writer, session)| {
        Future::join3(
          reader
            .take(1)
            .into_future()
            .map_err(|(err, _)| Error::from(err)),
          future::ok(writer),
          future::ok(session),
        )
      })
      .map(move |((message, reader), writer, mut session)| {
        let mut buf = [0u8; 65535];

        let message = match message {
          Some(NoiseMessage::Bytes(bytes)) => bytes,
          _ => panic!("What did you DO"),
        };

        session.read_message(&message, &mut buf).unwrap();

        NoiseTransport {
          session: session.into_transport_mode().unwrap(),
          reader: reader.into_inner(),
          writer,
        }
      })
  }

  pub fn listen<T: 'static>(
    addr: &SocketAddr,
    get_session: T,
  ) -> impl Stream<Item = NoiseTransport, Error = Error>
  where
    T: Fn() -> Session,
  {
    let listener = TcpListener::bind(&addr).unwrap();
    listener
      .incoming()
      .map_err(Error::from)
      .and_then(|socket| {
        let (read, write) = socket.split();
        let reader = FramedRead::new(read, Codec::new());
        let writer = FramedWrite::new(write, Codec::new());

        Future::join(
          reader
            .take(1)
            .into_future()
            .map_err(|(err, _)| Error::from(err)),
          future::ok(writer),
        )
      })
      .and_then(move |((message, reader), writer)| {
        let mut session = get_session();
        let mut buf_read = [0u8; 65535];
        let mut buf_write = [0u8; 65535];

        let message = match message {
          Some(NoiseMessage::Bytes(bytes)) => bytes,
          _ => panic!("What did you DO"),
        };

        session.read_message(&message, &mut buf_read).unwrap();

        let len = session.write_message(&[], &mut buf_write).unwrap();

        Future::join3(
          future::ok(reader),
          writer
            .send(NoiseMessage::Bytes(Bytes::from(&buf_write[..len])))
            .map_err(Error::from),
          future::ok(session),
        )
      })
      .map(move |(reader, writer, session)| NoiseTransport {
        session: session.into_transport_mode().unwrap(),
        reader: reader.into_inner(),
        writer: writer,
      })
  }
}

impl Stream for NoiseTransport {
  type Item = NoiseMessage;
  type Error = Error;

  fn poll(&mut self) -> Poll<Option<Self::Item>, Self::Error> {
    let message = try_ready!(self.reader.poll());

    let mut buf_read = [0u8; 65535];

    println!("message from NoiseTransport stream: {:#?}", message);
    let message = match message {
      Some(NoiseMessage::Bytes(bytes)) => bytes,
      _ => return Ok(Async::NotReady),
    };

    let len = self.session.read_message(&message, &mut buf_read).unwrap();

    Ok(Async::Ready(Some(NoiseMessage::Bytes(Bytes::from(
      &buf_read[..len],
    )))))
  }
}

impl Sink for NoiseTransport {
  type SinkItem = NoiseMessage;
  type SinkError = Error;

  fn start_send(
    &mut self,
    item: Self::SinkItem,
  ) -> Result<AsyncSink<Self::SinkItem>, Self::SinkError> {
    let mut buf_write = [0u8; 65535];

    let message = match item {
      NoiseMessage::Bytes(bytes) => bytes,
      _ => panic!("What did you DO"),
    };

    println!("starting send in NoiseTransport sync of bytes {:#?}", message);
    let len = self
      .session
      .write_message(&message, &mut buf_write)
      .unwrap();

    self
      .writer
      .start_send(NoiseMessage::Bytes(Bytes::from(&buf_write[..len])))
      .map_err(Error::from)
  }

  fn poll_complete(&mut self) -> Poll<(), Self::SinkError> {
    self.writer.poll_complete().map_err(Error::from)
  }
}
