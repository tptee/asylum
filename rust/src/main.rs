#![warn(clippy::all)]
#![feature(custom_attribute)]

use ::actix::io::{FramedWrite, WriteHandler};
use ::actix::prelude::*;
use bytes::Bytes;
use failure::{Error, Fail};
use futures::future;
use futures::prelude::*;
use hex::decode;
use lazy_static::lazy_static;
use snow::params::NoiseParams;
use snow::Builder;
use std::env;
use std::io;
use std::net::SocketAddr;
use std::process;
use std::str::FromStr;
use std::sync::Arc;
use tokio_codec::FramedRead;
use tokio_io::io::WriteHalf;
use tokio_io::AsyncRead;
use tokio_tcp::TcpStream;

use asylum::codec::{Codec, CodecError, NoiseMessage};
use asylum::identity::{GetTransportKeypair, IdentityActor, IdentityError};
use asylum::transport::{NoiseTransport, NoiseTransportError};

lazy_static! {
  static ref NOISE_PARAMS: NoiseParams = "Noise_IK_25519_ChaChaPoly_BLAKE2s".parse().unwrap();
}

#[derive(Fail, Debug)]
#[fail(display = "An error occurred.")]
enum InitError {
  #[fail(display = "IO error: {}", _0)]
  Io(io::Error),
  #[fail(display = "Mailbox error: {}", _0)]
  Mailbox(MailboxError),
  #[fail(display = "Identity error: {}", _0)]
  Identity(IdentityError),
  #[fail(display = "Noise transport error: {}", _0)]
  NoiseTransport(NoiseTransportError),
  #[fail(display = "Unknown error")]
  Unknown,
}

enum ConnectionType {
  Dialer,
  Listener,
}

struct PeerConnected {
  addr: SocketAddr,
  stream: TcpStream,
  keypair: Arc<snow::Keypair>,
  connection_type: ConnectionType,
}
impl PeerConnected {
  fn new(
    addr: SocketAddr,
    stream: TcpStream,
    keypair: Arc<snow::Keypair>,
    connection_type: ConnectionType,
  ) -> PeerConnected {
    PeerConnected {
      addr,
      stream,
      keypair,
      connection_type,
    }
  }
}

impl Message for PeerConnected {
  type Result = Result<(), Box<std::error::Error>>;
}

struct NoisePeerConnected(NoiseTransport);
impl Message for NoisePeerConnected {
  type Result = Result<(), Error>;
}

struct Peer {
  connection_type: ConnectionType,
  keypair: Option<snow::Keypair>,
  session: Option<snow::Session>,
  writer: Option<FramedWrite<WriteHalf<TcpStream>, Codec>>,
}

impl Actor for Peer {
  type Context = Context<Self>;

  fn started(&mut self, _ctx: &mut Self::Context) {
    println!("Started peer");
  }

  fn stopped(&mut self, _ctx: &mut Self::Context) {
    println!("Stopped peer");
  }
}

impl WriteHandler<CodecError> for Peer {
  fn error(&mut self, err: CodecError, _ctx: &mut Self::Context) -> Running {
    println!("error in Codec WriteHandler: {:?}", err);
    Running::Stop
  }
}

// impl Handler<PeerConnected> for Peer {
//   type Result = Result<(), Box<std::error::Error>>;

//   fn handle(&mut self, msg: PeerConnected, ctx: &mut Context<Self>) -> Self::Result {
//     self.handle_connection(msg, ctx)
//   }
// }

// impl StreamHandler<PeerConnected, io::Error> for Peer {
//   fn handle(&mut self, msg: PeerConnected, ctx: &mut Context<Self>) {
//     let _ = self.handle_connection(msg, ctx);
//   }

//   fn error(&mut self, err: io::Error, _ctx: &mut Self::Context) -> Running {
//     println!("{:?}", err);
//     Running::Stop
//   }
// }

// new messages
impl StreamHandler<NoiseMessage, Error> for Peer {
  fn handle(&mut self, msg: NoiseMessage, ctx: &mut Context<Self>) {
    println!("Received NoiseMessage: {:x?}", msg);
  }

  fn error(&mut self, err: Error, _ctx: &mut Self::Context) -> Running {
    println!("error");
    println!("{:#?}", err);
    Running::Stop
  }
}

// client
impl Handler<NoisePeerConnected> for Peer {
  type Result = Result<(), Error>;

  fn handle(&mut self, msg: NoisePeerConnected, ctx: &mut Context<Self>) -> Self::Result {
    ctx.add_stream(msg.0);
    Ok(())
  }
}

// server
impl StreamHandler<NoisePeerConnected, Error> for Peer {
  // type Result = ResponseFuture<(), Error>;
  fn handle(&mut self, msg: NoisePeerConnected, ctx: &mut Context<Self>) {
    ctx.spawn(
      msg
        .0
        .send(NoiseMessage::Bytes(Bytes::from(&b"hell oh world"[..])))
        .and_then(|writer| writer.send(NoiseMessage::Bytes(Bytes::from(&b"oh hell world"[..]))))
        .map(|_| ())
        .map_err(|_| ())
        .into_actor(self),
    );
  }

  fn error(&mut self, err: Error, _ctx: &mut Self::Context) -> Running {
    println!("error in NoisePeerConnected handler: {:#?}", err);
    Running::Stop
  }
}

// impl StreamHandler<NoiseMessage, CodecError> for Peer {
//   fn handle(&mut self, msg: NoiseMessage, _ctx: &mut Context<Self>) {
//     let mut session = self.session.take().unwrap();
//     let message = match msg {
//       NoiseMessage::Bytes(bytes) => Some(bytes),
//       _ => None,
//     }
//     .unwrap();
//     let mut buf_msg = [0u8; 65535];
//     let mut buf_out = [0u8; 65535];

//     session.read_message(&message, &mut buf_msg).unwrap();

//     if session.is_handshake_finished() {
//       self.session = Some(session.into_transport_mode().unwrap());
//       println!("oh my god we're in transport mode");
//       return;
//     }

//     let len = session.write_message(&[], &mut buf_out).unwrap();
//     {
//       let writer = self.writer.as_mut().unwrap();
//       writer.write(NoiseMessage::Bytes(Bytes::from(&buf_out[..len])));
//     }

//     if session.is_handshake_finished() {
//       self.session = Some(session.into_transport_mode().unwrap());
//       println!("oh my god we're in transport mode");
//       return;
//     }

//     self.session = Some(session);
//     // println!("{:?}", session.is_handshake_finished());
//     // let len = session.write_message(&[], &mut buf_out).unwrap();
//     // {
//     //   let writer = self.writer.as_mut().unwrap();
//     //   writer.write(NoiseMessage::Bytes(Bytes::from(&buf_out[..len])));
//     // }
//     //   let mut buf = [0u8; 65535];

//     //   let _ = session.read_message(&message, &mut buf);
//     //   let len = session.write_message(&[], &mut buf).unwrap();
//     //   {
//     //     let writer = self.writer.as_mut().unwrap();
//     //     writer.write(NoiseMessage::Bytes(Bytes::from(&buf[..len])));
//     //   }
//     //   self.session = Some(session.into_transport_mode().unwrap());
//     // } else {
//     //   if session.is_handshake_finished() {
//     //     self.session = Some(session.into_transport_mode().unwrap());
//     //   } else {
//     //     self.session = Some(session);
//     //   }
//     // }
//   }

//   fn error(&mut self, err: CodecError, _ctx: &mut Self::Context) -> Running {
//     println!("{:?}", err);
//     Running::Stop
//   }
// }

impl Peer {
  fn new(connection_type: ConnectionType) -> Self {
    Peer {
      connection_type,
      session: None,
      keypair: None,
      writer: None,
    }
  }

  // fn handle_connection(
  //   &mut self,
  //   msg: PeerConnected,
  //   ctx: &mut Context<Peer>,
  // ) -> Result<(), Box<std::error::Error>> {
  //   let (read, write) = msg.stream.split();
  //   let framed_read = FramedRead::new(read, Codec::new());
  //   let mut writer = FramedWrite::new(write, Codec::new(), ctx);

  //   let builder = Builder::new(NOISE_PARAMS.clone());
  //   self.keypair = Some(builder.generate_keypair().unwrap());

  //   match msg.connection_type {
  //     ConnectionType::Dialer => {
  //       println!("Dialer");
  //       self.session = Some(
  //         builder
  //           .local_private_key(&msg.keypair.private)
  //           .remote_public_key(
  //             &decode("fcb3405a3be2fea06878b410789990ee4a7ab31c8ea05abf51be57b84cd1a73b")
  //               .expect("Bad public key hex"),
  //           )
  //           .build_initiator()
  //           .expect("could not create initiator session!"),
  //       );

  //       let mut noise_buf = vec![0u8; 65535];
  //       if let Some(ref mut session) = self.session {
  //         let len = session
  //           .write_message(b"wtf", noise_buf.as_mut_slice())
  //           .unwrap();
  //         writer.write(NoiseMessage::Bytes(Bytes::from(&noise_buf[..len])));
  //       }
  //     }
  //     ConnectionType::Listener => {
  //       println!("Listener");
  //       self.session = Some(
  //         builder
  //           .local_private_key(&msg.keypair.private)
  //           .remote_public_key(
  //             &decode("5ebfdd802a596e0802ede7d22caaa25dbaf50c0f7eab7a8f307266eb29522767")
  //               .expect("Bad public key hex"),
  //           )
  //           .build_responder()
  //           .expect("could not create responder session!"),
  //       );
  //     }
  //   }

  //   ctx.add_stream(framed_read);
  //   self.writer = Some(writer);

  //   Ok(())
  // }
}

fn server(addr: SocketAddr) {
  let file_server = Arbiter::start(|_| IdentityActor::new("server".into()));
  // let listener = TcpListener::bind(&addr).unwrap();

  Arbiter::spawn(
    file_server
      .send(GetTransportKeypair)
      .map_err(Error::from)
      .and_then(|keypair| keypair.map_err(Error::from))
      .and_then(move |keypair| {
        let get_session = move || {
          Builder::new(NOISE_PARAMS.clone())
            .local_private_key(&keypair.private)
            .remote_public_key(
              &decode("5ebfdd802a596e0802ede7d22caaa25dbaf50c0f7eab7a8f307266eb29522767")
                .expect("Bad public key hex"),
            )
            .build_responder()
            .expect("could not create responder session!")
        };

        Peer::create(move |ctx| {
          ctx.add_stream(NoiseTransport::listen(&addr, get_session).map(NoisePeerConnected));
          // ctx.add_stream(listener.incoming().map(move |stream| {
          //   let keypair = Arc::clone(&keypair);
          //   let addr = stream.peer_addr().unwrap();
          //   PeerConnected::new(addr, stream, keypair, ConnectionType::Listener)
          // }));
          println!("Starting server");
          Peer::new(ConnectionType::Listener)
        });
        futures::future::ok(())
      })
      .map_err(Error::from)
      .map_err(|e| {
        println!("Can not start server: {}", e);
        process::exit(1)
      }),
  );
}

fn client(addr: SocketAddr) {
  let file_server = Arbiter::start(|_| IdentityActor::new("client".into()));

  Arbiter::spawn(
    file_server
      .send(GetTransportKeypair)
      .map_err(Error::from)
      .and_then(|keypair| keypair.map_err(Error::from))
      .and_then(move |keypair| {
        let session = Builder::new(NOISE_PARAMS.clone())
          .local_private_key(&keypair.private)
          .remote_public_key(
            &decode("fcb3405a3be2fea06878b410789990ee4a7ab31c8ea05abf51be57b84cd1a73b")
              .expect("Bad public key hex"),
          )
          .build_initiator()
          .expect("could not create initiator session!");
        Future::join(
          NoiseTransport::connect(&addr, session).map_err(Error::from),
          future::ok(keypair),
        )
      })
      .and_then(|(transport, keypair)| {
        println!("we connected af");
        Peer::create(move |ctx| {
          ctx.notify(NoisePeerConnected(transport));
          // ctx.add_stream(transport.map(NoisePeerConnected));
          // ctx.add_stream(listener.incoming().map(move |stream| {
          //   let keypair = Arc::clone(&keypair);
          //   let addr = stream.peer_addr().unwrap();
          //   PeerConnected::new(addr, stream, keypair, ConnectionType::Listener)
          // }));
          println!("Starting client");
          Peer::new(ConnectionType::Listener)
        });

        future::ok(())
      })
      // .and_then(|(socket, keypair)| {
      //   println!("public {:x?}", keypair.public);
      //   Peer::create(|ctx| {
      //     let addr = socket.peer_addr().unwrap();
      //     ctx.notify(PeerConnected::new(
      //       addr,
      //       socket,
      //       Arc::new(keypair),
      //       ConnectionType::Dialer,
      //     ));
      //     Peer::new(ConnectionType::Dialer)
      //   });
      //   futures::future::ok(())
      // })
      .map_err(|e| {
        println!("Can not connect to server: {:#?}", e);
        process::exit(1)
      }),
  );
}

fn main() {
  let sys = System::new("asylum");
  let addr = SocketAddr::from_str("127.0.0.1:9999").unwrap();

  match env::args().nth(1).as_ref() {
    Some(arg) if arg == "server" => server(addr),
    Some(arg) if arg == "client" => client(addr),
    Some(arg) => panic!(format!("Must be server or client, passed: {}", arg)),
    _ => panic!("Must be server or client, no arguments passed"),
  }

  sys.run();
}
