'use strict';

import * as Vorpal from 'vorpal';
import * as hyperswarm from '@hyperswarm/network';

import LocalMember from './local-member';
import Conversation from './conversation';

const cli = new Vorpal();

const main = async () => {
  if (!process.env.MEMBER_NAME) {
    throw new Error('Missing member name!');
  }

  const network = hyperswarm();

  const localMember = await LocalMember.create({
    deviceId: process.env.MEMBER_NAME,
    network
  });

  localMember.announce();

  cli
    .command('show-discovery-key')
    .action(() =>
      Promise.resolve(
        cli.activeCommand.log(localMember.feed.discoveryKey.toString('hex'))
      )
    );

  cli.command('start-conversation <discoveryKey>').action(args => {
    const conversation = new Conversation({
      sessionId: 'test',
      localMember,
      remoteMemberDiscoveryKeys: [Buffer.from(args.discoveryKey, 'hex')],
      network
    });

    return new Promise(resolve => {
      // conversation.on('connection', resolve);
    });
  });

  cli.delimiter(process.env.MEMBER_NAME).show();
};

main().catch(err => {
  console.error(err);
  process.exit(1);
});
