import * as path from 'path';
import { promisifyAll } from 'bluebird';
import * as hyperdb from 'hyperdb';

export default class RemoteMember {
  feed: any;

  private constructor({ feed }: { feed: any }) {
    this.feed = feed;
  }

  public static async create({ id }: { id: string }) {
    const feed = promisifyAll(
      hyperdb(path.resolve(process.cwd(), id), { valueEncoding: 'binary' })
    );
    return new Promise((resolve: (member: RemoteMember) => void) => {
      const member = new RemoteMember({ feed });
      feed.on('ready', () => resolve(member));
    });
  }
}
