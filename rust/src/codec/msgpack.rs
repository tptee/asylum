use actix::Message;
use byteorder::{ByteOrder, NetworkEndian};
use bytes::{BufMut, BytesMut};
use rmp_serde as msgpack;
use std::io;
use tokio_codec::{Decoder, Encoder};

#[derive(Serialize, Deserialize, Debug)]
pub enum Request {
  Handshake,
  Op(String),
}

impl Message for Request {
  type Result = Result<bool, Box<std::error::Error>>;
}

#[derive(Serialize, Deserialize, Message, Debug)]
pub enum Response {
  Handshake,
  Op(String),
}

pub enum CodecError {
  Decode(msgpack::decode::Error),
  IO(io::Error),
}

impl From<io::Error> for CodecError {
  fn from(err: io::Error) -> Self {
    CodecError::IO(err)
  }
}

impl From<msgpack::decode::Error> for CodecError {
  fn from(err: msgpack::decode::Error) -> Self {
    CodecError::Decode(err)
  }
}

pub struct Codec;

impl Codec {
  pub fn new() -> Codec {
    Codec {}
  }
}

impl Decoder for Codec {
  type Item = Request;
  type Error = CodecError;

  fn decode(&mut self, src: &mut BytesMut) -> Result<Option<Self::Item>, Self::Error> {
    println!("decoding");
    let size = {
      if src.len() < 2 {
        return Ok(None);
      }
      println!("about to read length prefix");
      NetworkEndian::read_u16(src.as_ref()) as usize
    };

    if src.len() >= size + 2 {
      src.split_to(2);
      let buf = src.split_to(size);
      let req = msgpack::from_slice::<Request>(&buf)?;
      return Ok(Some(req));
    }
    Ok(None)
  }
}

impl Encoder for Codec {
  type Item = Request;
  type Error = Box<std::error::Error>;

  fn encode(&mut self, item: Self::Item, dst: &mut BytesMut) -> Result<(), Self::Error> {
    let msg = msgpack::to_vec(&item)?;

    dst.reserve(msg.len() + 2);
    dst.put_u16_be(msg.len() as u16);
    dst.put(msg);

    Ok(())
  }
}
