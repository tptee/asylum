import * as hyperswarm from '@hyperswarm/network';
class Registry {
  private network: typeof hyperswarm;

  constructor({ network }: { network: typeof hyperswarm }) {
    this.network = network;
  }
}
