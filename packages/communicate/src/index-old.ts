// // 'use strict';

// const sodium = require('libsodium-wrappers-sumo');
// const ram = require('random-access-memory');
// const { promisify } = require('util');
// const hyperdb = require('hyperdb');
// const { MemoryEngine } = require('@wireapp/store-engine');
// const { Cryptobox } = require('@wireapp/cryptobox');

// const aliceDb = hyperdb(ram, { valueEncoding: 'binary' });
// const bobDb = hyperdb(ram, { valueEncoding: 'binary' });

// aliceDb.put = promisify(aliceDb.put);
// aliceDb.get = promisify(aliceDb.get);
// bobDb.put = promisify(bobDb.put);
// bobDb.get = promisify(bobDb.get);

// const createEngine = async storeName => {
//   const engine = new MemoryEngine();
//   await engine.init(storeName);
//   return engine;
// };

// // eslint-disable-next-line max-statements
// const startDemo = async () => {
//   await sodium.ready;

//   const alice = {
//     desktop: new Cryptobox(await createEngine('alice_desktop'), 3)
//   };
//   const bob = {
//     desktop: new Cryptobox(await createEngine('bob_desktop'), 1),
//     mobile: new Cryptobox(await createEngine('bob_mobile'), 1)
//   };

//   const messageFromBob = 'Hello Alice!';

//   await Promise.all([
//     alice.desktop.create(),
//     bob.desktop.create(),
//     bob.mobile.create()
//   ]);

//   await aliceDb.put(
//     '/prekeys',
//     await alice.desktop.get_serialized_standard_prekeys().then(JSON.stringify)
//   );

//   const alicePreKeyNode = await aliceDb.get('/prekeys');
//   const alicePreKeyBundle = JSON.parse(
//     alicePreKeyNode[0].value.toString('utf8')
//   );

//   const decodedPreKeyBundleBuffer = sodium.from_base64(
//     alicePreKeyBundle[0].key,
//     sodium.base64_variants.ORIGINAL
//   ).buffer;

//   await bob.desktop.session_from_prekey('hi', decodedPreKeyBundleBuffer);
//   const ciphertext = await bob.desktop.encrypt('hi', messageFromBob);
//   const ciphertextBytes = new Uint8Array(ciphertext);

//   await bobDb.put(
//     `/conversations/${alice.desktop.identity.public_key.fingerprint()}`,
//     Buffer.from(ciphertextBytes)
//   );

//   const message = await bobDb.get(
//     `/conversations/${alice.desktop.identity.public_key.fingerprint()}`
//   );

//   const messageBuffer = message[0].value;
//   const messageArrayBuffer = messageBuffer.buffer.slice(
//     messageBuffer.byteOffset,
//     messageBuffer.byteOffset + messageBuffer.byteLength
//   );

//   const [, plaintext] = await alice.desktop.session_from_message(
//     'hi',
//     messageArrayBuffer
//   );

//   console.log(sodium.to_string(plaintext));
// };

// startDemo().catch(console.error.bind(console));
